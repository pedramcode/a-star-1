var astar;

function setup(){
    createCanvas(600,600);
    astar = new AStar(10,10);
    astar.set_block(1,1);
    astar.set_block(1,2);
    astar.set_block(1,3);
    astar.set_block(1,4);
    astar.set_block(2,1);
    astar.set_block(3,1);
    astar.set_block(4,1);
    astar.set_block(4,4);
    astar.set_block(4,5);
    astar.set_block(5,4);
    astar.set_block(5,5);
    astar.set_block(8,8);
    astar.set_block(8,7);
    astar.set_block(8,6);
    astar.set_block(8,5);
    astar.set_block(7,8);
    astar.set_block(6,8);
    astar.set_block(5,8);

    astar.set_start(1,8);
    astar.set_end(8,1);
}

function draw(){
    background(70,70,70);
    astar.render();
}