const BLOCK=1;
const START=2;
const END=3;
class AStar{
    constructor(_width, _height){
        this.x = _width;
        this.y = _height;
        this.start=undefined;
        this.end=undefined;
        this.matrix = []
        for(var i = 0 ; i < _height ; i++){
            var tmp = [];
            for(var j = 0 ; j < _width ; j++){
                tmp.push(0);
            }
            this.matrix.push(tmp);
        }
    }

    random_block(_num){
        for(var i = 0 ; i < _num ; i++){
            while(true){
                var block_x, block_y;
                block_x = int(random()*this.x);
                block_y = int(random()*this.y);
                if(this.matrix[block_y][block_x]==1){
                    continue;
                }
                this.matrix[block_y][block_x]=1;
                break;
            }
        }
    }

    set_block(_x,_y){
        this.matrix[_y][_x]=BLOCK;
    }

    set_start(_x,_y){
        this.matrix[_y][_x]=START;
        this.start=[_x,_y];
    }

    set_end(_x,_y){
        this.matrix[_y][_x]=END;
        this.end=[_x,_y];
    }

    draw_lines(){
        push();
        strokeWeight(1);
        stroke(255);
        for(var i = 0 ; i < width ; i+=width/this.x){
            line(i,0,i,height);
        }
        for(var i = 0 ; i < height ; i+=width/this.y){
            line(0,i,width,i);
        }
        pop();
    }

    fill_pos(_x,_y,_color){
        push();
        var cell_size = int(width/this.x);
        noStroke();
        fill(_color[0],_color[1],_color[2]);
        rect(_x*cell_size,_y*cell_size,cell_size,cell_size);
        pop();
    }

    render(){
        push();
        this.draw_lines();
        for(var i = 0 ; i < this.y ; i++){
            for(var j = 0 ; j < this.x ; j++){
                if(this.matrix[i][j]==BLOCK){
                    this.fill_pos(j,i,[0,0,0]);
                }
                else if(this.matrix[i][j]==START){
                    this.fill_pos(j,i,[255,0,0]);
                }
                else if(this.matrix[i][j]==END){
                    this.fill_pos(j,i,[0,255,0]);
                }
            }
        }
        pop();
    }
}